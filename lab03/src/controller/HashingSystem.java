package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.Hashing;
import view.SoftwareFrame;


public class HashingSystem{

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashingSystem controller = new HashingSystem();
		controller.setModel(new Hashing());
		controller.setFrame(new SoftwareFrame());
		controller.setHashingListener();
		

	}
	
	public void setModel(Hashing hashing){
		this.hashing = hashing;
	}
	public void setFrame(SoftwareFrame frame){
		this.frame = frame;
	}
	
	public void setHashingListener(){
		
		frame.getButton().addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int ascii = hashing.toASCII(frame.getTextTxT());
				int n = Integer.parseInt(frame.getTextNTxT());
				//int n = 4;
				int mod = hashing.hash(ascii,n);
				frame.setTextResult1(ascii+"");
				frame.setTextResult2(mod+"");
			}
		}
		);
			
		
	}

	private SoftwareFrame frame;
	private Hashing hashing;
}
