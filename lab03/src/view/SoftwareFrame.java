package view;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class SoftwareFrame extends JFrame {
   public SoftwareFrame() {
	   createFrame();
	   
   }
   public void createFrame() {
	   
	   setBounds(500,150,400,150);
       
	   setTitle("Hashing");
	   txt = new JTextField();
	   ntxt = new JTextField();
	   word = new JLabel("word :");
	   ngram = new JLabel("ngram :");
	   ascii = new JLabel("ASCII : ");
	   hash = new JLabel("Hash : ");
	   button = new JButton("generate");
	   result1 = new JLabel();
	   result2 = new JLabel();
	   
	   txt.setBounds(50,10,120,20);
	   word.setBounds(10, 10, 50, 20);
	   ntxt.setBounds(50,40,120,20);
	   ngram.setBounds(10, 40, 50, 20);
	   button.setBounds(180,10,90,20);
	   ascii.setBounds(10,70,50,20);
	   result1.setBounds(65,70,80,20);
	   hash.setBounds(10,90,50,20);
	   result2.setBounds(65, 90, 80, 20);
	   

	   setLayout(null);
	   
	   add(txt);
	   add(word);
	   add(ntxt);
	   add(ngram);
	   add(button);
	   add(ascii);
	   add(hash);
	   add(result1);
	   add(result2);
	   
	   

	   setVisible(true);
	   setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	   
   }
   
   public String getTextTxT(){
	   return txt.getText();
   }
   
   public String getTextNTxT(){
	   return ntxt.getText();
   }
   
   public JButton getButton(){
	   return button;
   }
   
   public void setTextResult1(String result1) {
	   this.result1.setText(result1);
   }
   
   public void setTextResult2(String result2) {
	   this.result2.setText(result2);
   }





   private JTextField txt;
   private JTextField ntxt;
   private JLabel word;
   private JLabel ngram;
   private JLabel ascii;
   private JLabel hash;
   private JButton button;
   private JLabel result1;
   private JLabel result2;

} 
