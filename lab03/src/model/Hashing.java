package model;

public class Hashing {
	
	public int toASCII(String word){
		int len = word.length();
		int sum = 0;
		for(int i=0;i<len;i++){
			sum += word.charAt(i);
		}
		
		return sum;
	}
	
	public int hash(int ascii,int n){
		ascii %= n;
		return ascii;
	}
}
